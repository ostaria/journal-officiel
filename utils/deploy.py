#!/usr/bin/env python3

import os
import re
import datetime
from subprocess import call

datePattern = re.compile("\d\d?(er)? +[A-Za-zâàéèêëîïôöùû]* +\d\d\d?")

maxBackgroundColorNumber = 9

def sortSubdirs(p, force_head):
    nextSubdirs = [p]
    while len(nextSubdirs) != 0:
        next = nextSubdirs.pop(0)
        for path, subdirs, files in os.walk(next):
            l = [(lastDate(path + "/" + s, s, force_head), path + "/" + s) for s in subdirs]
            l = sortDates(l)
            l = [c[1] for c in l]
            nextSubdirs = l + nextSubdirs
            l = [os.path.basename(c) for c in l]
            yield path, l, files
            break

def lastDate(p, s, force_head):
    if (s == force_head):
        return "top" #We put it at the top
    currentYear = 2001
    currentMonth = 1
    currentDay = 1
    for path, subdirs, files in os.walk(p):
        for f in files:
            date = ""
            with open(path + "/" + f) as file:
                for l in file.readlines():
                    for match in re.finditer(datePattern, l):
                        date = match.group()
            if date == "":
                continue
            date = parseDate(date).split("-")
            year, month, day = int(date[0]), int(date[1]), int(date[2])
            if (year > currentYear or (year == currentYear and month > currentMonth) or (year == currentYear and month == currentMonth and day > currentDay)):
                currentYear, currentMonth, currentDay = year, month, day
    return str(currentYear) + "-" + str(currentMonth) + "-" + str(currentDay)

def parseDate(date):
    dateSplit = date.lower().split()
    if len(dateSplit) < 3:
        return "2001-01-01"
    month = ""
    day = "01" if dateSplit[0] == "1er" else dateSplit[0]
    match dateSplit[1]:
        case "janvier":
            month = "01"
        case "février":
            month = "02"
        case "mars":
            month = "03"
        case "avril":
            month = "04"
        case "mai":
            month = "05"
        case "juin":
            month = "06"
        case "juillet":
            month = "07"
        case "août":
            month = "08"
        case "septembre":
            month = "09"
        case "octobre":
            month = "10"
        case "novembre":
            month = "11"
        case "décembre":
            month = "12"
    return "2" + dateSplit[2] + "-" + month + "-" + day

def sortDates(l):
    hasTop = False
    for i in range(len(l)):
        if l[i][0] == "top":
            hasTop = True
            top = l.pop(i)
            break
    dates = [(datetime.datetime.strptime(date, "%Y-%m-%d"), fileElem) for (date, fileElem) in l]
    dates.sort(reverse=True)
    sortedElems = [top] if hasTop else []
    return sortedElems + [(datetime.datetime.strftime(date, "%Y-%m-%d"), fileElem) for (date, fileElem) in dates]

def main():
    #Reads the config file
    with open("config") as config:
        for l in config.readlines():
            if l.startswith("gitlab_path="):
                gitlab_path = l[12:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("index_path="):
                index_path = l[11:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("default_branch="):
                default_branch = l[15:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("forum_link="):
                forum_link = l[11:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("image_main_page="):
                image_main_page = l[16:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("title_tab="):
                title_tab = l[10:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("title_main_page="):
                title_main_page = l[16:].replace("'", "\'").replace("\"", "\\")[:-1]
            elif l.startswith("force_head="):
                force_head = l[11:].replace("'", "\'").replace("\"", "\\")[:-1]

    #Creates public directory and subdirectories
    if not(os.path.isdir("../public")):
        os.mkdir("../public")
    if not(os.path.isdir("../public/Textes")):
        os.mkdir("../public/Textes")
    for path, subdirs, files in os.walk("../Textes"):
        file_path = "../public" + path[2:]
        if not(os.path.isdir(file_path)):
            os.mkdir(file_path)

    #Adds CSS file
    call("cp ./parser/xbbcode.css ../public/xbbcode.css", shell=True)
    css_file_name = "xbbcode.css"

    #Adds favicon
    call("cp favicon.ico ../public/favicon.ico", shell=True)

    #Parses files
    for path, subdirs, files in os.walk("../Textes"):
        for file in files:
            if file[-7:] != ".bbcode":
                continue
            file_path = path + "/" + file
            css_file_path = css_file_name
            favicon_path = "favicon.ico"
            for i in range(file_path.count("/") - 1):
                css_file_path = "../" + css_file_path
                favicon_path = "../" + favicon_path
            public_file_path = "../public" + path[2:] + "/" + file[:-7] + ".html"
            remote_bbcode_path = gitlab_path + "-/blob/" + default_branch + file_path[2:] + "?ref_type=heads"
            remote_history_path = gitlab_path + "-/commits/" + default_branch + file_path[2:] + "?ref_type=heads"
            remote_blame_path = gitlab_path + "-/blame/" + default_branch + file_path[2:] + "?ref_type=heads"
            call("node parser_launcher.js \"" + file_path + "\" \"" + css_file_path + "\" \"" + remote_bbcode_path + "\" \"" + remote_history_path + "\" \"" + remote_blame_path + "\" \"" + index_path + "\" \"" + file[:-7] + "\" \"" + favicon_path + "\"", shell=True)
            call("mv parsed.html \"" + public_file_path + "\"", shell=True)

    #Creates index
    backgroundColorNumber = 0
    index = '<!DOCTYPE html><html><head><link rel="stylesheet" href="xbbcode.css">' \
            + '<title>' + title_tab + '</title>' \
            + '<link rel="icon" type="image/x-icon" href="favicon.ico"></head>' \
            + '<body><div style="text-align:center">' \
            + '<div class="links-block">' \
            + '<p class="link"><a href="' + forum_link + '">Retourner au forum</a></p>' \
            + '<p class="link"><a href="search.html">Rechercher</a></p>' \
            + '<p class="link"><a href="' + gitlab_path + '">Voir les sources</a></p>' \
            + '</div><br>' \
            + '<img alt="Drapeau" src="' + image_main_page + '" decoding="async" width="280" height="170" /><div><span style="color:#000000;;font-size:50px">' + title_main_page + '<big>&#160;</big></span></div>' \
            + '</div>' \
            + '<button id="openAll">Tout ouvrir</button><button id="closeAll">Tout fermer</button>' \
            + '<div><table class="main-table"><tr><th></th><th class="last-modification-header">Date de dernière modification</th></tr><tr>'
    listSubdirsEnd = []
    for path, subdirs, files in sortSubdirs("../Textes", force_head):
        listAsTree = []
        date = ""
        if path == "../Textes":
            continue
        backgroundColorNumber = min(maxBackgroundColorNumber, backgroundColorNumber + 1)
        index += "<tr><td colspan=\"2\" class=\"collapsible active\">" + os.path.basename(path) + "</td></tr><tr><td colspan=\"2\" class=\"content visible-content\"><table class=\"background" + str(backgroundColorNumber) + "\">"
        if len(subdirs) != 0:
            listSubdirsEnd.append(path + "/" + subdirs[-1])
        for f in files:
            if f[-7:] != ".bbcode":
                continue
            #Gets the date of the last modification
            with open(path + "/" + f) as file:
                for l in file.readlines():
                    for match in re.finditer(datePattern, l):
                        date = match.group()
            fileElem = "<tr><td><div class=\"index-law\"><div class=\"index-link\"><a href = \"" + path + "/" + f[:-7] + ".html\">" + f[:-7] + "</a></div>" \
                    + ("<td class=\"last-modification\">" + date + "</td></div></div></td></tr>" if date != -1 else "<td class=\"last-modification\">?</td></div></div></td></tr>")
            listAsTree.append((parseDate(date), fileElem))
        sortedElems = sortDates(listAsTree)
        for date, elem in sortedElems:
            index += elem
        if len(subdirs) == 0:
            index += "</div></table></td></tr>"
            backgroundColorNumber = max(0, backgroundColorNumber - 1)
        if path in listSubdirsEnd:
            listSubdirsEnd.remove(path)
            index += "</div></table></td></tr>"
            backgroundColorNumber = max(0, backgroundColorNumber - 1)
    index += "</div></table></div>" + """ \
              <script>
              const coll = document.getElementsByClassName("collapsible");

              for (let i = 0; i < coll.length; i++) {
                  coll[i].addEventListener("click", function() {
                      this.classList.toggle("active");
                      const content = this.parentElement.nextSibling.getElementsByClassName("content")[0];
                      content.classList.toggle("visible-content");
                      content.classList.toggle("invisible-content");
                  });
              }

              document.getElementById("openAll").addEventListener("click", function() {
                  const coll = document.getElementsByClassName("collapsible");
                  for (let i = 0; i < coll.length; i++) {
                      coll[i].classList.add("active");
                  }
                  const collContents = document.getElementsByClassName("invisible-content");
                  while (collContents.length !== 0) {
                      collContents[0].classList.toggle("visible-content");
                      collContents[0].classList.toggle("invisible-content");
                  }
              });

              document.getElementById("closeAll").addEventListener("click", function() {
                 const coll = document.getElementsByClassName("active");
                 while (coll.length !== 0) {
                     coll[0].classList.remove("active");
                 }
                 const collContents = document.getElementsByClassName("visible-content");
                 while (collContents.length !== 0) {
                     collContents[0].classList.toggle("invisible-content");
                     collContents[0].classList.toggle("visible-content");
                 }
              });

              </script>""" + "</body></html>"
    with open("../public/index.html", "w") as f:
        f.write(index)

    #Creates search page
    searchPage = '<!DOCTYPE html><html><head><link rel="stylesheet" href="xbbcode.css">' \
                   + '<title>' + title_tab + '</title>' \
                   + '<link rel="icon" type="image/x-icon" href="favicon.ico">' \
                   + '<link href="/pagefind/pagefind-ui.css" rel="stylesheet">' \
                   + '<script src="/pagefind/pagefind-ui.js"></script>' \
                   + '<div id="search"></div>' \
                   + '<script>' \
                   + 'window.addEventListener(\'DOMContentLoaded\', (event) => {' \
                   + 'const search = new PagefindUI({ element: "#searchResults", showSubResults: true });' \
                   + 'document.getElementById("searchResults").appendChild(search);' \
                   + '});' \
                   + '</script></head>' \
                   + '<body><div style="text-align:center">' \
                   + '<div class="links-block">' \
                   + '<p class="link"><a href="' + index_path + '">Retourner à l\'index</a></p>' \
                   + '<p class="link"><a href="' + forum_link + '">Retourner au forum</a></p>' \
                   + '<p class="link"><a href="' + gitlab_path + '">Voir les sources</a></p>' \
                   + '</div><br></div>' \
                   + '<div id="searchResults"></div>' \
                   + '</body></html>'
    with open("../public/search.html", "w") as searchFile:
        searchFile.write(searchPage)

main()