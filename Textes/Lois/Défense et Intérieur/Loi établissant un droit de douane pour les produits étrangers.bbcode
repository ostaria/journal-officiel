[quote][center][b][size=170]Loi établissant un droit de douane pour les produits étrangers[/size][/b][/center]



[size=150][b][u]Titre Ier - Définitions[/u][/b][/size]

[b]Article 1.-[/b]
Est défini dans le présent texte un produit étranger un produit dont la production n'ayant pas été faite intégralement sur le territoire ostarien et/ou n'ayant pas été conçu uniquement par une ou des personnes n'ayant pas la nationalité ostarienne.
Est défini dans le présent texte un produit ostarien un produit dont la production ayant été faite intégralement sur le territoire ostarien et uniquement par des personnes possédant la nationalité ostarienne.

[b]Article 2.-[/b]
Les salaires médians cités ci-après sont issus de chiffres attestés par les autorités ostariennes. Dans le cas où une nation ne permettrait pas aux autorités ostariennes d'attester de la réalité du salaire médian annoncé, les produits issus de ces pays se verront taxés à la plus grande tranche existante.

[b]Article 3.-[/b]
La liste des produits qu'Ostaria ne produit pas en quantité suffisante afin de permettre à la nation d'être en autarcie est communiquée par le ministère du Développement Durable tous les deux mois.


[size=150][b][u]Titre II - Des produits figurant sur la liste[/u][/b][/size]

[b]Article 4.-[/b]
Les produits figurant sur la liste précisée à l'article 3 sont soumis à un droit de douanes selon le salaire médian des personnes ayant contribué à créer les produits concernés. Ce droit de douanes est progressif. Le barème en est déterminé à l'article 5.

[b]Article 5.-[/b]
Le barème du droit de douanes pour les produits ne figurant pas sur la liste précisée à l'article 3 est établi comme suit :
    - De - 15% à - 23,5% du salaire médian ostarien : 8,5% du prix de vente
    - De - 23,5% à - 32% du salaire médian ostarien : 17% du prix de vente
    - De - 40,5% à - 49% du salaire médian ostarien : 25,5% du prix de vente
    - De - 49% à - 57,5% du salaire médian ostarien : 34% du prix de vente
    - De - 57,5% à - 66% du salaire médian ostarien : 42,5% du prix de vente
    - De - 66% à - 74,5% du salaire médian ostarien : 51% du prix de vente
    - De - 74,5% à - 83% du salaire médian ostarien : 59,5% du prix de vente
    - De - 83% à - 91,5% du salaire médian ostarien : 68% du prix de vente
    - Moins de 8,5% du salaire médian ostarien : 80% du prix de vente

[b]Article 6.-[/b]
Les taux déterminés à l'article 5 sont cumulables avec les autres taxes sur le prix de vente.


[size=150][b][u]Titre III - Des produits ne figurant pas sur la liste[/u][/b][/size]

[b]Article 7.-[/b]
Les produits ne figurant pas sur la liste précisée à l'article 3 sont également soumis au droit de douanes déterminé aux articles 4, 5 et 6.

[b]Article 8.-[/b]
Les produits ne figurant pas sur la liste doivent, en plus du droit de douanes déterminé aux articles 4, 5 et 6, payer à l'État ostarien l'intégralité du coût de production de ce produit en cas de mise en vente, et ce quel que soit le salaire médian des personnes ayant participé à sa production.




[right]Promulgué le [b]11 avril 152[/b] à Lunont

[b]Jérôme Plassel[/b],
[i]Président de la République d’Ostaria[/i].[/right][/quote]