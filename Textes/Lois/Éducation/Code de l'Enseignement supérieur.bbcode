[quote][center][b][size=170]Code de l’Enseignement supérieur[/size][/b][/center]



[size=150][b][u]Titre Ier - Principes fondamentaux[/u][/b][/size]

[size=130][b]Chapitre Ier - Dispositions générales[/b][/size]

[b]Article 101-1.-[/b]
L’État garantit l’égal accès à l’Université Nationale d’Ostaria pour tous les Ostariens et toutes les Ostariennes, sans distinction de sexe, d’origine, de situation sociale ou de croyance, à compter de l’obtention du Baccalauréat Ostarien ou diplôme équivalent.

[b]Article 101-2.-[/b]
L’équivalence de diplôme fait l’objet d'un accord signé par le ministre chargé de l’enseignement supérieur avec un autre État.
Nul ne peut, en dehors du ministre chargé de l’enseignement supérieur ou d’un autre État, mettre fin à l’équivalence d’un diplôme.

[b]Article 101-3.-[/b]
Aucun étudiant, qu’il soit inscrit à l’Université Nationale d’Ostaria ou dans une université privée sous contrat, ne peut faire l’objet de discrimination ou de limitation de ses droits.

[b]Article 101-4.-[/b]
L’Université Nationale d’Ostaria est gratuite. Les frais d’inscriptions sont intégralement pris en charge et les fournitures nécessaires au bon déroulement des études sont délivrées sans frais aux étudiants inscrits.
Chaque année, une liste de fournitures considérées comme nécessaires est communiquée par le ministre chargé de l’enseignement supérieur. Chaque composante de l’Université Nationale d’Ostaria devra répertorier les besoins en fourniture de chaque étudiant.

[b]Article 101-5.-[/b]
L’Université Nationale d’Ostaria est laïque. Aucun signe religieux ne doit y être visible. Son personnel pédagogique et administratif est soumis à un stricte devoir de neutralité religieuse, et aucune croyance religieuse ne peut être exprimée.

[b]Article 101-6.-[/b]
L’enseignement supérieur est neutre en matière d’opinions politiques et philosophiques.

[b]Article 101-7.-[/b]
L’enseignement supérieur vise à développer la connaissance et l’innovation dans un environnement de recherche, d’échange et de liberté académique.
La liberté académique est garantie, permettant à chaque enseignant et chercheur d’exprimer ses convictions intellectuelles et scientifiques sans censure, dans le respect de la loi.

[b]Article 101-8.-[/b]
L’accès à l’enseignement supérieur pour les étudiants en situation de handicap est un droit fondamental. 
Chaque établissement d’enseignement supérieur doit être équipé des aménagements suffisants pour assurer l’accès des étudiants handicapés aux infrastructures et aux ressources pédagogiques.
Les universités doivent veiller à fournir un soutien adapté, incluant des dispositifs technologiques et humains.

[size=130][b]Chapitre II - Financement de l’enseignement supérieur[/b][/size]

[b]Article 102-1.-[/b]
L’Université Nationale d’Ostaria est intégralement financée par l’État. L’État garantit en ce sens l’allocation de ressources suffisantes pour le bon fonctionnement des établissements, l’acquisition de matériel et le développement de projets de recherche.

[b]Article 102-2.-[/b]
Les universités privées sous contrat disposent d’une autonomie financière. Elles peuvent percevoir des frais d’inscription et des frais de scolarité.
En complément des frais engagés par les étudiants, les universités privées sous contrat peuvent solliciter des fonds auprès d’organismes privés, à condition que leurs actions respectent l’éthique universitaire et les lois en vigueur. Ces organismes privés n’ont aucun droit de gestion sur ces types d’universités.
Seul le personnel pédagogique perçoit un salaire de l’État.


[size=150][b][u]Titre II - L’Université Nationale d’Ostaria[/u][/b][/size]

[b]Article 200-1.-[/b]
En plus des institutions étrangères dont l'équivalence des diplômes ferait l'objet d'un accord en vertu des dispositions établies dans le présent texte, seule l'UNO et, en son nom, les établissements d'enseignement supérieur sous contrat sont habilités à délivrer des diplômes, des titres et des formations universitaires qui sont reconnus par la Répubique d’Ostaria.

[b]Article 200-2.-[/b]
L’organisation et le fonctionnement de l’Université Nationale d’Ostaria sont définis dans la loi relative à la réforme de l’Université Nationale d’Ostaria.


[size=150][b][u]Titre III - L’enseignement supérieur privé sous contrat[/u][/b][/size]

[size=130][b]Chapitre Ier - Les universités privées sous contrat[/b][/size]

[b]Article 301-1.-[/b]
Les universités privées sous contrat en République d’Ostaria sont des établissements d’enseignement supérieur privés ayant signé un accord avec l’État, leur permettant de délivrer des diplômes reconnus par l’Université Nationale d’Ostaria.
Ce statut leur confère l’autorisation de participer au système national d’enseignement supérieur tout en conservant une certaine autonomie financière et pédagogique.

[b]Article 301-2.-[/b]
Les universités privées sous contrat suivent les règles académiques définies par l’État. Ils dispensent les formations validées par l’Université Nationale d’Ostaria, pour en garantir la qualité académique.
Les diplômes et les titres universitaires relatifs aux formations dispensées sont délivrés par l’Université Nationale d’Ostaria et disposent alors d’une reconnaissance de l’État.

[b]Article 301-3.-[/b]
Les universités privées sous contrat sont soumises, annuellement, à des contrôles de l’Université Nationale d’Ostaria pour s’assurer du respect des objectifs de formation.

[b]Article 301-4.-[/b]
En cas de non-respect des conditions du contrat entre l’État et l’université privée, celle-ci peut se voir retirer son contrat, suspendant dès lors, la délivrance de diplôme de l’Université Nationale d’Ostaria.
Le premier alinéa du présent article ne dispose pas d’effet rétroactif et n’annule donc pas les diplômes déjà délivrés ni les diplômes délivrés après que le contrat est retiré, au terme d'études ayant débuté alors que l'établissement était sous contrat.
Pour retrouver un contrat avec l’État, l’université privée devra se plier aux exigences du présent titre.

[size=130][b]Chapitre II - Liberté pédagogique et subventions publiques[/b][/size]

[b]Article 302-1.-[/b]
Les universités privées sous contrat disposent d’une autonomie pédagogique dans l’élaboration de leurs programmes d’études, tant que ceux-ci permettent la validation du diplôme et de la formation à l’issue du cursus universitaire.
Elles peuvent définir leurs méthodes d’enseignement, sélectionner leurs enseignants et fixer leurs priorités de recherche.

[b]Article 302-2.-[/b]
À condition que les activités de recherche s’inscrivent dans les missions de recherche de l’Université Nationale d’Ostaria, des subventions publiques peuvent être débloquées pour les universités privées sous contrat.


[size=150][b][u]Titre IV - L’enseignement supérieur privé hors contrat[/u][/b][/size]

[b]Article 400-1.-[/b]
Les établissements d’enseignement supérieur hors contrat ne sont pas habilités à prendre le nom d’université ou à délivrer un titre universitaire. Les éventuels diplômes délivrés par un établissement d'enseignement supérieur hors contrat ne peuvent être reconnus par l'État.

[b]Article 400-2.-[/b]
Les diplômes délivrés par les établissements hors contrat ne bénéficient d’aucune reconnaissance officielle en matière de qualifications académiques ou professionnelles en Ostaria.
Sont exemptés de cette disposition les diplômes délivrés au terme d'études au début desquelles ils étaient voués à être reconnus comme équivalents aux diplômes de l'UNO.

[b]Article 400-3.-[/b]
Les établissements hors contrat sont soumis à la loi en matière de sécurité et de respect des droits des étudiants. Ils ne peuvent prétendre à aucune subvention ou partenariat avec l’enseignement supérieur public et privé sous contrat.


[size=150][b][u]Titre V - La recherche universitaire[/u][/b][/size]

[b]Article 500-1.-[/b]
La recherche universitaire est une mission fondamentale de l’enseignement supérieur ostarien. L’Université Nationale d’Ostaria et les universités privées sous contrat sont des centres de production et de diffusion de la connaissances scientifiques, technologiques, sociales et médicales.

[b]Article 500-2.-[/b]
Le financement de la recherche est assuré par l’État et des partenariats privés. L’Université Nationale d’Ostaria doit promouvoir l’excellence scientifique et l’interdisciplinarité, en encourageant les collaborations internationales et la valorisation des résultats de recherche.




[right]Promulgué le [b]26 octobre 230[/b] à Lunont

[b]Christophe Letordu[/b],
[i]Président de la République d’Ostaria[/i].[/right][/quote]