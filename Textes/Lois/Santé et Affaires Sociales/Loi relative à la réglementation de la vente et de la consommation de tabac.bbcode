[quote][center][b][size=170]Loi relative à la réglementation de la vente et de la consommation de tabac[/size][/b][/center]



[i]Constatant l’étendue des conséquences néfastes de la consommation de tabac pour la société, l’Assemblée Nationale reconnaît la nécessité de réglementer plus efficacement la consommation et la vente de tabac.[/i]


[size=150][b][u]Titre Ier - Des dispositions transitoires[/u][/b][/size]

[b]Article 101.-[/b]
L’article 4 de la loi sur le tabac et les stupéfiants du 23 février 150 est abrogée.

[b]Article 102.-[/b]
Sont considérés dans la présente loi comme étant des tabacs manufacturés : les cigares, les cigarettes, le tabac fine coupe destiné à rouler les cigarettes, le tabac à priser et le tabac à mâcher.

[b]Article 103.-[/b]
L’ensemble des dispositions de la présente loi ne faisant pas l’object de mention spécifique sur leur date d’application s’appliquent dès la publication au Journal officiel.


[size=150][b][u]Titre II - Du permis d’exploitation[/u][/b][/size]

[b]Article 201.-[/b]
La vente de tabacs manufacturés sur le territoire national est réservée à des personnes morales disposant d’un permis d’exploitation spécifique décerné par le Ministère en charge de l’Économie.

[b]Article 202.-[/b]
Le permis d’exploitation ne peut être attribué à une personne morale dont l’exploitant est mineur ou a été condamné par une Cour de justice dans les 15 ans précédant l’attribution.

[b]Article 203.-[/b]
La vente de tabacs manufacturés par une personne physique ou morale ne disposant du permis d’exploitation est considérée comme un trafic de stupéfiants en grande quantité.

[b]Article 204.-[/b]
Le Ministère en charge de l'Économie peut refuser d'accorder un permis d'exploitation à une personne morale en faisant la demande lorsque ladite personne ne satisfait pas aux exigences évoquées à l'article 202 du présent texte.
Il peut aussi le refuser s'il existe un doute raisonnable sur la capacité ou la volonté de ladite personne de garantir que la production s'effectue dans des conditions d'hygiène, de santé et de sécurité suffisantes, ou que la nocivité du produit sera réduite dans la limite du possible.
Enfin, il peut le refuser s'il existe un doute raisonnable sur la capacité ou la volonté de ladite personne à respecter la législation et la réglementation en vigueur, et, en particulier, s'il existe un doute raisonnable sur des activités illicites passées, présentes ou prévues de ladite personne en lien avec le trafic de stupéfiants.
Tout refus d'attribution du permis d'exploitation parmi le Ministère en charge de l'Économie doit être motivé et faire l'objet d'une communication en précisant le motif.
Tout refus est contestable devant une Cour du justice.

[b]Article 205.-[/b]
Toute personne morale commercialisant des tabacs manufacturés au moment de la promulgation de la présente loi peuvent continuer à le faire à titre exceptionnel sans permis d’exploitation pendant une durée de deux ans.

[b]Article 206.-[/b]
Le permis d’exploitation peut être retiré par le Ministère en charge de l’Économie avec un préavis de 10 jours en cas de manquement manifeste à la réglementation des commerces autorisés.
Toute décision en ce sens est contestable devant une Cour de justice.


[size=150][b][u]Titre III - Des conditions d’achat[/u][/b][/size]

[b]Article 301.-[/b]
La vente de tabacs manufacturés n’est possible qu’à des personnes âgées d’au moins 18 ans révolus.

[b]Article 302.-[/b]
La vente de tabac manufacturés doit être précédée d’un contrôle par le vendeur du remplissage des conditions fixées à l’article 301 de la présente loi, au moyen du contrôle d’une pièce d’identité.

[b]Article 303.-[/b]
La vente de tabac manufacturés à une personne ne remplissant pas les conditions fixées à l’article 301 de la présente loi entraîne la responsabilité légale du vendeur pour les éventuels dommages causés par la vente.


[size=150][b][u]Titre IV - De la réglementation de la consommation[/u][/b][/size]

[b]Article 401.-[/b]
La consommation de tabac manufacturés est prohibée dans les bâtiments publics et les moyens de transports.

[b]Article 402.-[/b]
Toute personne morale ou physique est autorisée à décider de l’interdiction de la consommation de tabac manufacturés sur un bien qu’il possède.

[b]Article 403.-[/b]
La consommation de tabac manufacturés dans des zones où elle est prohibée est passible d’une amende de 50 000 O$tas.




[right]Promulgué le [b]21 mai 213[/b] à Lunont

[b]François Pelichon[/b],
[i]Président de la République d’Ostaria[/i].[/right][/quote]