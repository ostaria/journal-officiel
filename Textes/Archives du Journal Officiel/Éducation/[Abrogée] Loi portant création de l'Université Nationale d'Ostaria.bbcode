
[center][i]LOI ABROGÉE PAR LA LOI RELATIVE À LA RÉFORME DE L'UNIVERSITÉ NATIONALE D'OSTARIA (12 JUIN 214)[/i][/center]

————————————————————————————————————————————————————————————————————————————————————————————————

[quote][center][b][size=170]Loi portant création de l'Université Nationale d'Ostaria[/size][/b][/center]



[size=150][b][u]Titre Ier - Dispositions générales[/u][/b][/size]

[b]Article 1.-[/b]
L’intégralité des établissements publics d’enseignement supérieur et de recherche sont fusionnés dans un Etablissement Public d’Enseignement et de Recherche Scientifiques appelé « Université Nationale d’Ostaria ».

[b]Article 2.-[/b]
L’Université Nationale d’Ostaria a le monopole de l’enseignement supérieur et le monopole de la collation des grades universitaires de licence, maitrise et doctorat sur le territoire ostarien.

[b]Article 3.-[/b]
L’Université Nationale d’Ostaria est une université confédérale composées de Facultés, d’Instituts Universitaires et de Composantes Administratives et de leurs services. Chacune des Facultés, Instituts Universitaires ou composantes administratives dispose de sa propre gouvernance.

[b]Article 4.-[/b]
Les Ecoles Pratiques Ostariennes sont des établissements spécifiques dépendants de l’Université Nationale d’Ostaria. Ce sont les seuls établissements universitaires accueillant des étudiants n’ayant pas le baccalauréat ostarien pour y passer ce diplôme. Les Ecoles Pratiques Ostariennes sont également chargée de l’instruction des citoyens ostariens récemment naturalisés. Une Ecole Pratique Ostarienne est installée dans chacune des capitales régionales.

[b]Article 5.-[/b]
Les formations universitaires sont accessibles à toutes personnes titulaires du baccalauréat ostarien ou d’un diplôme ostarien ou étranger reconnu comme équivalent par le Ministère en charge de l’Education.
Les formations universitaires se répartissent autour de trois cursus : la licence suivie de la maitrise elle-même suivie par le doctorat.

[b]Article 6.-[/b]
Le passage d’un cursus à l’autre n’est pas automatique. Les titulaires d’une licence de l’Université Nationale d’Ostaria peuvent candidater dans une maitrise d’une discipline similaire ou connexe selon ce que la réglementation prescrit. Les titulaires d’une maitrise de l’Université Nationale d’Ostaria peuvent solliciter une bourse doctorale et intégrer un cursus doctoral.
L’accès aux grades universitaires supérieurs étant volontaire et sélectif selon le niveau d’exigence académique fixé par les responsables des formations et la réglementation nationale.


[size=150][b][u]Titre II - Du service public de l’enseignement supérieur et de la recherche[/u][/b][/size]

[b]Article 7.-[/b]
Les domaines de l’enseignement supérieur et de la recherche passent et demeurent sous monopole d’Etat. L’Université Nationale d’Ostaria reçoit en délégation du gouvernement une mission de service public d’enseignement supérieur et de recherche.
L’Université Nationale d’Ostaria dispose du monopole de la collation des grades universitaires. Les diplômes de l’Université sont garantis par le gouvernement et reçoivent la qualification de Diplôme d’Etat.
Le Diplôme d’Etat du Baccalauréat constitue le premier diplôme de l’enseignement supérieur. Son organisation et son évolution font l’objet d’une collaboration entre les Ministères chargés de l’Education et celui chargé de l’Enseignement Supérieur et de la Recherche.

[b]Article 8.-[/b]
L’Université Nationale d’Ostaria est un Etablissement Public. Les fonctionnaires qui y travaillent ainsi que ses usagers sont astreints au devoir de réserve. La laïcité des enseignements et des institutions universitaires sont garantis. Les personnels et les usagers sont tenus aux mêmes devoirs de neutralité politique et philosophiques en cours dans les établissements scolaires régis par le Code de l’Education.

[b]Article 9.-[/b]
Les personnels de l’Université Nationale d’Ostaria sont majoritairement constitués de fonctionnaires d’Etat. Le Chancelier-Recteur de l’Université est toutefois autorisé à recruter des personnels contractuels pour assurer la mission de service public d’enseignement supérieur. Ces personnels contractuels ne pourront toutefois pas demeurer employés sous ce statut pendant plus de dix années.

[b]Article 10.-[/b]
L’Université Nationale d’Ostaria est dépositaire d’une mission de service public en assurant l’instruction supérieure des étudiants ostariens et des étudiants étrangers admis à suivre une formation.
Le monopole d’Etat concédé à l’Université Nationale d’Ostaria ne peut faire l’objet d’une délégation de compétences à des entités privées, sauf dérogation exceptionnelle accordée par Arrêté du Ministre en charge de l’Enseignement Supérieur et de la Recherche.


[size=150][b][u]Titre III - Des diplômes universitaires[/u][/b][/size]

[b]Article 11.-[/b]
L’Université Nationale d’Ostaria attribue à titre ordinaire les diplôme universitaires d’Etat et attribue durant les sessions extraordinaires le Diplôme d’Etat du Baccalauréat Ostarien.

[b]Article 12.-[/b]
La licence est le premier grade universitaire au-delà du Baccalauréat. Il justifie de la maitrise des savoirs généraux et techniques d’une discipline universitaire.
Les modalités d’admission, de suivi, de construction du diplôme et du contrôle général des connaissances des formations de licence font l’objet d’un Arrêté du Ministre en charge de l’Enseignement Supérieur et de la Recherche.

[b]Article 13.-[/b]
La maîtrise est le deuxième grade universitaire après le Baccalauréat. Il s’obtient dans un cursus de deux années après une licence. Il justifie d’une connaissance de spécialiste d’une discipline universitaire.
L’accès à une formation de type maîtrise est conditionnée à la possession d’une licence de l’Université Nationale ou d’un diplôme jugé équivalent au sens de l’article 5 de la présente loi.
Les modalités d’admission, de suivi, de construction du diplôme et du contrôle général des connaissances des formations de maitrise font l’objet d’un Arrêté du Ministre en charge de l’Enseignement Supérieur et de la Recherche. Le Chancelier-Rectoral et le Conseil Rectoral sont obligatoirement sollicités avant toute promulgation d’Arrêtés relatifs aux formations de maitrise.

[b]Article 14.-[/b]
Le doctorat est le dernier des grades universitaires. Il s’obtient trois années après l’obtention d’une maitrise universitaire de l’Université Nationale ou d’un diplôme jugé équivalent au sens de l’article 5 de la présente loi.
Le titre de Docteur de l’Université Nationale d’Ostaria s’acquiert en soutenant publiquement une thèse dont l’apport est jugé comme déterminant au développement de la connaissance et de la recherche. La soutenance publique est dirigée par un collège de professeurs de l’Université Nationale.
Les modalités d’admission, de suivi, de construction du diplôme et du contrôle général des connaissances des études doctorales font l’objet d’un Arrêté du Ministre en charge de l’Enseignement Supérieur et de la Recherche. Le Chancelier-Rectoral et le Conseil Rectoral sont obligatoirement sollicités avant toute promulgation d’Arrêtés relatifs aux études doctorales.

[b]Article 15.-[/b]
Les Conseils de Facultés et d’Instituts sont habilités à construire et présenter les maquettes des formations. Le Conseil Rectoral en assure la validation définitive. Le Ministère en charge de l’Enseignement Supérieur et de la Recherche assure le suivi et le contrôle des actes pédagogiques du Conseil Rectoral afin de veiller au respect de la législation et de la réglementation.

[b]Article 16.-[/b]
Le Conseil Rectoral fixe, après avis des conseils de facultés et d’instituts, la répartition des grands secteurs de formation.


[size=150][b][u]Titre IV - Des composantes universitaires et des laboratoires de recherches[/u][/b][/size]

[b]Article 17.-[/b]
L’Université Nationale d’Ostaria est subdivisée en plusieurs types d’entités administratives appelée « composantes universitaires » :
    - Les Facultés
    - Les Instituts Universitaires
    - Les Collèges Universitaires
    - Les Centres de Recherches Universitaires
    - Les Centres Hospitaliers Universitaires
    - Les Ecoles Pratiques Ostariennes
Chacune de ces composantes est chargée d’une mission d’enseignement et/ou de recherche et bénéficie d’un patrimoine dévolu, d’un budget, d’une administration et de service propres ou collectivisés.
Le patrimoine et le budget de ces entités administratives est dévolu par le Conseil Rectoral de l’Université Nationale d’Ostaria selon ce que les Statuts de l’Université prescrit.

[b]Article 18.-[/b]
Les composantes de l’Université Nationale d’Ostaria sont dirigées par des Doyens, assistés d’un ou plusieurs Vice-Doyens et d’un Conseil de Composante.
Les composantes regroupent des formations d’un ou plusieurs cursus universitaires et accueillent un centre de recherche.
Une composante peut être elle-même subdivisée en départements universitaires le cas échéant. La création des départements universitaires sont validés par un acte du Conseil de la composante concernée.
Les modalités de nomination des Doyens de composante et d’élection des Conseils de composantes sont précisées dans un Arrêté du Ministre en charge de l’Enseignement Supérieur et de la Recherche.

[b]Article 19.-[/b]
L’Université Nationale d’Ostaria compte quatre facultés :
    - La Faculté de Droit et de Science Politique installée à Lunont
    - La Faculté des Lettres, des Langues, des Arts et des Sciences de l’Homme installée à Bridame
    - La Faculté des Sciences et Techniques installée à Condail
    - La Faculté de Médecine et des Sciences de la Santé installée à Illonlieu

[b]Article 20.-[/b]
Chaque capitale régionale comporte une Ecole Pratique Ostarienne, un Institut Universitaire de Formation des Maitres, un Centre Hospitalier Universitaire.

[b]Article 21.-[/b]
L’Université Nationale d’Ostaria compte huit composantes cogérées entre le Ministère en charge de l’Enseignement Supérieur et de la Recherche et d’autres ministères :
    - Les six Centres Hospitaliers Universitaires installés dans les six capitales régionales qui sont cogérée avec le Ministère en charge de la santé
    - Le Collège Universitaire des Armées installé à Tasasque est cogéré avec le Ministère en charge de la défense
    - Le Conservatoire Ostarien des Arts et Métiers installé à Tuse est cogéré avec le Ministère en charge du Travail

[b]Article 22.-[/b]
L’Université Nationale d’Ostaria assure la cogestion des Centres Hospitaliers Universitaires avec le Ministère en charge de la Santé. Un Arrêté des ministres en charge de l’Enseignement Supérieur et de la Recherche et de la Santé organisent le fonctionnement des CHU.

[b]Article 23.-[/b]
Les composantes administratives de l’Université Nationale d’Ostaria établissent les statuts organisant leur fonctionnement interne. Les statuts de composantes doivent être conformes à la loi et à la réglementation et ne peuvent contrevenir aux termes des Statuts de l’Université Nationale d’Ostaria.

[b]Article 24.-[/b]
Le Collège Universitaire des Armée est un établissement d’Enseignement Supérieur et de la Recherche associé à l’Université Nationale d’Ostaria et relevant du Ministère en charge de la défense et du Haut-Commandement des Armées. Il forme les cadres militaires et civils de la défense.
L’organisation et le fonctionnement du Collège Universitaire des Armées fait l’objet d’un Arrêté conjoint entre le Ministre en charge de la Défense et le Ministre en charge de l’Enseignement Supérieur et de la Recherche.

[b]Article 25.-[/b]
Chaque faculté dispose également d’un laboratoire de recherche et d’une école doctorale qui lui sont attachés. Les doyens et conseils de facultés gèrent la politique de recherche et de formation doctorale en ce que les statuts de la faculté et de l’Université prescrivent.

[b]Article 26.-[/b]
La Faculté de Médecine et de Sciences de la Santé d’Illonlieu est liée par convention aux six Centres Hospitaliers Universitaires répartis dans le territoire national. Les CHU permettent d’accueillir les étudiants de médecine durant leurs phases de formations en milieu hospitalier. Les CHU forment les étudiants en médecine et des diverses spécialités médicales et paramédicales aux réalités des métiers de la santé.
Ces CHU bénéficient de dotations supplémentaires en praticiens, en personnels et en moyens permettant l’encadrement optimal des étudiants.

[b]Article 27.-[/b]
Un Centre Universitaire Expérimental accueillant les matériels lourds et les infrastructures des départements universitaires pouvant être mutualisés avec les centres de recherches militaires, aéronautiques et spatiaux est établi et installé à Tasasque.
Un Arrêté du Chancelier-Recteur de l’Université Nationale d’Ostaria fixe le fonctionnement et précise les missions du Centre Universitaire Expérimental de Tasasque qui dépend directement du Rectorat de l’Université.

[b]Article 28.-[/b]
Un Centre Spatial et un Cosmodrome sont établis et installés à Irrosque et dépend de la Faculté des Sciences et Technique. Ce centre réunit les départements universitaires et les infrastructures de recherche et techniques lié au domaine spatial.

[b]Article 29.-[/b]
Les Instituts Universitaires de Formation des Maitres sont des composantes installées dans toutes les capitales régionales et ayant pour mission de recruter et de former les instituteurs et les professeurs du secondaire.

[b]Article 30.-[/b]
Le Conservatoire Ostarien des Arts et Métiers est une composante administrative spécialisée dans l’enseignement professionnel et technique supérieur. Ses formations sont spécialisées dans les domaines de l’artisanat, des formations professionnelles des secteurs primaires, secondaires et tertiaires. Le Conservatoire est ouvert aux personnes titulaires du Certificat d’Etudes Professionnelles.
Un Arrêté du Ministre en charge du Travail et du Ministre en charge de l’Enseignement Supérieur et de la Recherche organise le fonctionnement du Conservatoire et fixe les modalités d’admission.

[b]Article 31.-[/b]
Toute création d’une nouvelle composante administrative de l’Université Nationale d’Ostaria après la promulgation de la présente loi sera validée par une délibération du Conseil Rectoral après avis favorable du Ministère en charge de l’Enseignement Supérieur et de la Recherche.


[size=150][b][u]Titre V - Du statut des enseignants et des chercheurs[/u][/b][/size]

[b]Article 32.-[/b]
Les enseignants de l’Université Nationale d’Ostaria sont nommés après avis du Ministère en charge de l’Enseignement Supérieur et de la Recherche par arrêté du Chancelier-Recteur sur proposition du Conseil Rectoral et des Doyens des composantes recevant les enseignants nommés.

[b]Article 33.-[/b]
Les enseignants de l’Université Nationale d’Ostaria sont répartis dans trois grades :
    - Les Professeurs d’Université
    - Les Maitres de Conférences
    - Les Maitres-assistants
Le grade de Professeur d’Université est accessible aux docteurs et chercheurs dont la carrière dépasse les dix années.
Le grade de Maitre de conférences est accessible aux docteur et chercheurs enseignant depuis l’obtention de leur doctorat et jusqu'à dix ans.
Le grade de Maitre-assistant est accessible aux titulaires d’une maitrise engagé ou non dans un cursus doctoral.

[b]Article 34.-[/b]
Les enseignants des trois grades sont recrutés sur concours organisés par l’Université Nationale d’Ostaria sous contrôle du Ministère en charge de l’Enseignement Supérieur et de la Recherche.
Les modalités des concours sont établies par Arrêté ministériel.

[b]Article 35.-[/b]
L’Université Nationale d’Ostaria peut recruter des enseignants contractuels sur un poste d’enseignant. Un enseignant contractuel ne pourra recevoir un statut et une rémunération supérieure à celle d’un Maitre de conférences et ne pourra être employé plus de 5 ans au titre d’enseignant contractuel.

[b]Article 36.-[/b]
Les enseignants sont représentés dans les instances de composante et au sein du Conseil Rectoral. Ils bénéficient également du droit de se syndiquer. Un Arrêté du Conseil Rectoral précise les modalités d’expression des droits syndicaux.

[b]Article 37.-[/b]
Au même titre que les usagers, les enseignants de l’Université Nationale d’Ostaria s’astreignent aux mêmes obligations de neutralité que les enseignants de l’instruction primaire et secondaire.


[size=150][b][u]Titre VI - De l’organisation et de la gouvernance universitaires[/u][/b][/size]

[b]Article 38.-[/b]
L’Université Nationale d’Ostaria est un Etablissement Public d’Enseignement et de Recherche Scientifiques. Elle dispose de la personnalité juridique et d’une autonomie dans la gestion de ses affaires courantes et internes. Le Ministère en charge de l’Enseignement Supérieur et de la Recherche assure le contrôle de ses activités.
Le Président de la République est Grand-Maitre de l’Université Nationale d’Ostaria et garantit par là, la protection publique de l’Université, des Arts et des Sciences.

[b]Article 39.-[/b]
La direction de l’Université Nationale d’Ostaria est assurée par un Chancelier-Recteur élu par le Conseil Rectoral de l’Université pour une durée de 10 ans renouvelable.
Le Chancelier-Recteur préside le Conseil Rectoral et peut nommer jusqu'à deux Vice-recteurs pour l’assister dans ses fonctions.

[b]Article 40.-[/b]
Le Chancelier-Recteur doit être enseignant de l’Université Nationale, titulaire d’un doctorat depuis au moins trois ans et être de nationalité ostarienne pour pouvoir être candidat.

[b]Article 41.-[/b]
Le Chancelier-Recteur pourvoit aux emplois publics et contractuels en consultation avec les Doyens ou les chefs de services concernés.

[b]Article 42.-[/b]
Le Conseil Rectoral est l’organe délibérant de l’Université Nationale d’Ostaria. Il est composé des doyens de composantes, de cinq professeurs par composantes élus par leurs pairs, de quatre représentants des étudiants par composantes élus par leurs pairs et de six représentants des personnels non-enseignants.

[b]Article 43.-[/b]
Le Conseil Rectoral est en charge de l’administration de l’Université. Il élit le Chancelier-Recteur, affecte le budget, donne les grandes orientations académiques, contrôle et valide les décisions prises par les conseils de composante et contrôle la gestion des affaires courantes.
Le Conseil Rectoral peut déléguer tout ou une partie de ses attributions au Chancelier-Recteur ou aux conseils de composantes par vote d’une délibération habilitante par le Conseil.

[b]Article 44.-[/b]
Les mandats des Conseillers Rectoraux – hors représentants des étudiants – est de dix ans renouvelables.
Les Conseillers Rectoraux issus du corps étudiant sont élus pour un mandat de deux ans renouvelable.

[b]Article 45.-[/b]
Les conseils de composantes sont composés de dix représentants des enseignants, dix représentants des étudiants et cinq représentants des personnels non-enseignants pour un mandat de 3 ans renouvelable.


[size=150][b][u]Titre VII - De la coopération internationale[/u][/b][/size]

[b]Article 46.-[/b]
L’Université Nationale d’Ostaria se dote d’un service en charge des affaires internationales avec pour mission de développer et gérer des relations avec des universités et établissements d’enseignement supérieur étrangers.

[b]Article 47.-[/b]
L’Université Nationale d’Ostaria est autorisée à conclure des conventions entre établissements d’enseignement supérieur et de recherche situé dans des pays reconnus par la République d’Ostaria. Ces accords entre établissements doivent être validés par le Conseil Rectoral et le Ministère en charge des Affaires Extérieures.

[b]Article 48.-[/b]
Les accords entre établissements portent sur des programmes d’échanges d’étudiants ou de professeurs, la création de formations co-habilitées, la reconnaissance mutuelle des titres et diplômes ou l’établissement d’équivalences et enfin sur le partage ou l’échange de compétences et de connaissances.

[b]Article 49.-[/b]
Les accords entre établissements doivent être conformes à la législation ostarienne. Tout accord ou conséquences d’un accord contrevenant à la loi ostarienne rend ce dernier nul et non avenu.

[b]Article 50.-[/b]
En attente de la constitution d’une direction de l’Université conforme aux termes de la présente loi et en attente de la rédaction des Statuts de l’Université, le Premier Ministre nomme un Administrateur Provisoire en charge de mettre en œuvre la fusion des établissements et assurer la création des nouvelles structures.
L’Administrateur Provisoire assurera les pouvoirs du Chancelier-Recteur, il désigne un Conseil Rectoral Provisoire. L’Administration Provisoire est nommée pour au moins dix années au terme desquelles l’organisation ordinaire de l’Université Nationale devra être mise en œuvre.




[right]Promulgué le [b]20 août 168[/b] à Lunont

[b]Jérôme Plassel[/b],
[i]Président de la République d’Ostaria[/i].[/right][/quote]