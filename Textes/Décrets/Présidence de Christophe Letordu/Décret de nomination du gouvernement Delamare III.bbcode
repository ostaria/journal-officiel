[quote][center][size=170][b]Décret de nomination du gouvernement Delamare III[/b][/size][/center]



[b]Article premier :[/b] Le gouvernement Delamare III est nommé comme suit :
- Premier Ministre en charge de la Coopération gouvernementale et de l'Intérieur : Marius Delamare (LA)
- Vice-Premier Ministre en charge de la Justice et de la Défense des droits : Ernest Vabre-Nicolo (LNC)
- Ministre d'État chargé des Affaires étrangères et de l'Union Phoécienne : Christophe Bossu (LA)
- Ministre d'État chargé de l'Économie, des Finances et des Industries : Aloïse Lejeune (CP)
- Ministre des Armées : Swann Defontour (ADN)
- Ministre du Travail, de l'Emploi et de la Reconversion professionnelle : Marion Thévenoud (CP)
- Ministre de l'Éducation et de la Jeunesse : Antoine du Berry (ADN)
- Ministre de la Ruralité, de l'Aménagement et de la Cohésion des territoires : Claude Marcelli (LNC)
- Ministre de la Santé et de la Santé mentale : Armand Lornet (CP)
- Ministre de l'Enseignement supérieur et de la Recherche : Pascal Fauch (LA)
- Ministre de la Transition énergétique et de l'Environnement : Victoire Brunet (LNC)
- Ministre des Solidarités et du Logement : Jacques Dacapy (CP)
- Ministre de la Culture et des Sports : Elias Staphenberg (LA)
- Ministre des Transports et des Infrastructures : Arnaud Brassard (ADN)
- Secrétaire d'État à la Citoyenneté auprès du Premier Ministre en charge de la Coopération gouvernementale et de l'Intérieur : Neal Evans (ADN)
- Secrétaire d'État au Renouveau constitutionnel et démocratique auprès du Premier Ministre en charge de la Coopération gouvernementale et de l'Intérieur : Emmanuel Léger (LA)
- Secrétaire d'État aux Droits des minorités et à l'Égalité auprès du Vice-Premier Ministre en charge de la Justice et de la Défense des droits : Flora Nabin (CP)
- Secrétaire d'État à l'Union Phoécienne auprès du Ministre d'État chargé des Affaires étrangères et de l'Union Phoécienne : Ursula Ulfrieck (LNC)
- Secrétaire d'État au Budget et aux Finances auprès de la Ministre d'État chargé de l'Économie, des Finances et des Industries : Alice Lecomte (ADN)

[b]Article deuxième :[/b] Les compétences des membres du gouvernement sont établies comme suit :
- Premier Ministre en charge de la Coopération gouvernementale et de l'Intérieur : la présidence du Conseil des ministres, la coordination de la majorité, la coordination de l'action gouvernementale, la stabilité institutionnelle, la sécurité intérieure, la nationalité, la citoyenneté, l'organisation des élections, les relations avec le Parlement, les affaires cultuelles, l'équipement et la formation des forces de police, la direction des forces de l’ordre, la lutte contre toutes formes de violences et les affaires relatives à l'immigration
- Vice-Premier Ministre en charge de la Justice et de la Défense des droits : le fonctionnement de la justice, l'administration pénitentiaire et les libertés publiques, la lutte contre les discriminations, le droit des minorités, la lutte pour l'égalité de tous, le progrès sur les questions sociétales, la souveraineté numérique et la protection des données personnelles
- Ministre d'État chargé des Affaires étrangères et de l'Union Phoécienne : les relations internationales, la représentation au sein des organisations et des évènements internationaux, le commerce extérieur, la gestion du réseau diplomatique et du réseau d'ambassades et de consulats, la supervision des opérations militaires, les affaires relatives aux Ostariens expatriés ou travaillant à l'étranger et la coopération phoécienne
- Ministre d'État chargé de l'Économie, des Finances et des Industries : le budget, la fiscalité, la monnaie, le développement économique, les finances, la gestion des industries et la gestion des services publics
- Ministre des Armées : l'organisation et la mise en oeuvre de la défense nationale, l'organisation et la mise en oeuvre de la sécurité nationale à l'intérieur des frontières, l'équipement et la formation des forces armées et de la Gendarmerie Ostarienne et les douanes
- Ministre du Travail, de l'Emploi et de la Reconversion professionnelle : l'organisation des prestations sociales, le travail et le retour à l'emploi, le droit du travail, la formation professionnelle et l'organisation des négociations entre partenaires sociaux
- Ministre de l'Éducation et de la Jeunesse : les affaires relevant de la jeunesse, les affaires relevant du domaine de l'éducation, la gestion des écoles et la diversité de l'information
- Ministre de la Ruralité, de l'Aménagement et de la Cohésion des territoires : les affaires relatives aux collectivités territoriales, l'organisation du lien entre les territoires, les affaires relatives à la ruralités et à ses spécificités, la gestion de la cohésion des territoires et à l'aménagement du territoire
- Ministre de la Santé et de la Santé mentale : la santé publique, les politiques de santé et de santé mentale
- Ministre de l'Enseignement supérieur et de la Recherche : la gestion des universités, les questions relatives à l'enseignement supérieur, l'organisation et la gestion de la recherche et de l’innovation
- Ministre de la Transition énergétique et de l'Environnement : la planification de la transition écologique, les affaires énergétiques, les affaires environnementales, les affaires relatives à la politique agricole, à la gestion des sols et à la protection animale
- Ministre des Solidarités et du Logement : l’organisation des prestations sociales, la sécurité sociale, les politiques sociales, la modernisation des logements sociaux, la rénovation et la gestion des divers logements
- Ministre de la Culture et des Sports : la gestion des affaires culturelles, les affaires sportives, la promotion de la diversité et le rayonnement culturel
- Ministre des Transports et des Infrastructures : les affaires relatives aux transports, la gestion des infrastructures du pays, la modernisation des équipements
- Secrétaire d'État à la Citoyenneté auprès du Premier Ministre en charge de la Coopération gouvernementale et de l'Intérieur : la nationalité et la citoyenneté
- Secrétaire d'État au Renouveau constitutionnel et démocratique auprès du Premier Ministre en charge de la Coopération gouvernementale et de l'Intérieur :  les relations avec le Parlement et la stabilité institutionnelle
- Secrétaire d'État aux Droits des minorités et à l'Égalité auprès du Vice-Premier Ministre en charge de la Justice et de la Défense des droits : la lutte contre les discriminations, le droit des minorités, la lutte pour l'égalité de tous, la lutte contre les violences sexistes, le droit des femmes et le progrès sur les questions sociétales
- Secrétaire d'État à l'Union Phoécienne auprès du Ministre d'État chargé des Affaires étrangères et de l'Union Phoécienne : la coopération phoécienne
- Secrétaire d'État au Budget et aux Finances auprès de la Ministre d'État chargé de l'Économie, des Finances et des Industries : le budget, la fiscalité et les finances[/quote]




[right]Promulgué le [b]15 novembre 230[/b] à Lunont

[b]Christophe Letordu[/b],
[i]Président de la République d’Ostaria[/i].[/right][/quote]
